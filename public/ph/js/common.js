layui.use(['element', 'form', 'layer', 'jquery', 'upload', 'carousel', 'laydate', 'flow'], function () {
    var element = layui.element,
        form = layui.form,
        upload = layui.upload,
        flow = layui.flow,
        layer = layui.layer,
        laydate = layui.laydate,
        $ = layui.jquery;

    // debug
    form.on('switch(debug)', function (data) {
        ajax({field: {status: data.elem.checked}}, '/ph/index/debug.html');
    });
    // 图片懒加载
    flow.lazyimg({elem: '.cover img'});
    // 日期
    laydate.render({
        elem: '#create_time' //指定元素
        , type: 'datetime'
    });
    //执行实例
    uploadInst = upload.render({
        elem: '#upload' //绑定元素
        , url: '/ph/Api/upload' //上传接口
        , size: '2048'//文件最大可允许上传的大小
        , field: 'image' //设定文件域的字段名
        , done: function (res) {
            if (res.code === 200) {
                layer.msg(res.message, {icon: 1, time: 2000}, function () {
                    $("#litPic").val(res.data);
                    $("#imgShow").removeClass("layui-hide").attr("src", res.data);
                });
            } else {
                layer.msg(res.message, {icon: 2, time: 2000});
            }
        }
        , error: function () {
            layer.msg('接口错误', {icon: 2, time: 2000});
        }
    });
    //执行实例
    uploadInst = upload.render({
        elem: '#we_chat' //绑定元素
        , url: '/ph/Api/upload' //上传接口
        , size: '2048'//文件最大可允许上传的大小
        , field: 'image' //设定文件域的字段名
        , done: function (res) {
            if (res.code === 200) {
                layer.msg(res.message, {icon: 1, time: 2000}, function () {
                    $("#litPic1").val(res.data);
                    $("#imgShow1").removeClass("layui-hide").attr("src", res.data);
                });
            } else {
                layer.msg(res.message, {icon: 2, time: 2000});
            }
        }
        , error: function () {
            layer.msg('接口错误', {icon: 2, time: 2000});
        }
    });
    //执行实例
    map = upload.render({
        elem: '#map' //绑定元素
        , url: '/ph/Api/map' //上传接口
        , size: 10485760//文件最大可允许上传的大小
        , exts: 'xml|html|txt'
        , field: 'map' //设定文件域的字段名
        , done: function (res) {
            if (res.code === 200) {
                layer.msg(res.message, {icon: 1, time: 2000}, function () {
                    window.location.reload();
                });
            } else {
                layer.msg(res.message, {icon: 2, time: 2000});
            }
        }
        , error: function () {
            layer.msg('接口错误', {icon: 2, time: 2000});
        }
    });

    /**
     * 公共删除
     */
    $(".button-del").click(function () {
        var db = $(this).data("db");
        var id = $(this).data("id");
        if (!db || !id) {
            layer.msg("参数获取失败");
        }
        layer.open({
            title: "确认删除",
            content: "确认删除ID为 " + id + " 的数据吗？",
            btn: ['确定', '取消'],
            yes: function (index, layero) {
                ajax({field: {id: id}}, '/ph/' + db + '/data_delete.html');
                return false;
            }
            , btn2: function (index, layero) {
                layer.msg("已取消");
            }
        });
    });

    //退出
    $("#loginOut").click(function () {
        ajax('', '/ph/login/out.html', '', 1);
        return false;
    });

    //监听公共的提交事件
    form.on('submit(*)', function (data) {
        ajax(data, data.field.request_url);
        return false; //阻止表单跳转
    });

    /**
     * 公共ajax
     * @param data 数据
     * @param url 请求地址
     */
    function ajax(data, url) {
        if (!url) {
            layer.msg("接口地址缺失");
            return false;
        }
        $.ajax({
            url: url,
            type: 'post',
            data: data.field,
            dataType: 'json',
            async: false,
            success: function (info) {
                if (info.code === 200) {
                    layer.msg(info.message, {icon: 1, time: 2000}, function () {
                        if (info.jumpAddress) {
                            window.location.href = info.jumpAddress;
                        } else {
                            window.location.reload();
                        }
                    });
                } else {
                    layer.msg(info.message, {icon: 2, time: 2000});
                }
            },
            error: function () {
                layer.msg('接口错误', {icon: 2, time: 2000});
            }
        });
    }

    //图片预览
    $('.uploadImg').on('click', function () {
        var info = $(this).attr("src");
        layer.open({
            type: 1,
            shade: 0,
            title: false, //不显示标题
            content: "<img src=" + info + " class='openImg'>", //捕获的元素
        });
        return false;
    });

    // 历史封面图
    $("#history").click(function () {
        layer.open({
            title: '历史封面图片',
            type: 2,
            content: '/ph/content/img.html',
            area: ['350px', '100%'],
            offset: 'rt'
        });
        return false
    });

    // 历史图片复制地址
    $(".history-img").click(function () {
        $("#img").val($(this).data("src"))
        var text = $('#img');
        text.select();
        document.execCommand("Copy");
        layer.msg('复制成功');
        return false;
    });

    // 随机字符串
    $("#rand").click(function () {
        var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        var maxPos = $chars.length;
        var pwd = '';
        for (var i = 0; i < 8; i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        $("#path").val(pwd);
        return false;
    })
});
