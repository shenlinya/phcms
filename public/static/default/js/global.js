// 新闻资讯滚动
jQuery(".txtScroll-top").slide({
    titCell:".hd ul",
    mainCell:".bd ul",
    autoPage:true,
    effect:"top",
    autoPlay:true,vis:6
});
// 产品中心滚动
jQuery(".slideGroup .slideBox").slide({
    mainCell:"ul",
    vis:6,
    prevCell:".sPrev",
    nextCell:".sNext",
    effect:"leftMarquee",
    interTime:50,
    autoPlay:true,
    trigger:"click"
});
// 返回顶部
$(function(){
    $(window).scroll(function(){
        if($(window).scrollTop()>100){
            $(".gotop").fadeIn();
        }else{
            $(".gotop").hide();
        }
    });
    $(".gotop").click(function(){
        $('html,body').animate({
            'scrollTop':0
        },500);
    });
});
/* 域名*/
var http = location.protocol;
/* 协议*/
var yuming = window.location.host;
// 搜索
$(".sousuo-btn").click(function(){
    if($("input[name='keyowrd']").val() == ""){
    $("input[name='keyowrd']").focus();
    return false;
}else{
    window.open(http + "//" + yuming + "/index.php/Index/search.html?keyowrd=" + $("input[name='keyowrd']").val());
    }
});
if($(".up").attr('href')=="/index.php/news/info.html"){
    $(".up").css("display","none");
}
if($(".down").attr('href')=="/index.php/news/info.html"){
    $(".down").css("display","none");
}

