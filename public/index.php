<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

// PHP版本检测
if (PHP_VERSION < '5.6') {
    exit('您服务器PHP的版本太低，程序要求PHP版本不小于5.6.0');
}

// 检测是否是新安装
if (!file_exists(__DIR__ . '/install.lock')) {
    // 组装安装url
    $url = $_SERVER['HTTP_HOST'] . trim($_SERVER['SCRIPT_NAME'], 'index.php') . 'install.php';
    header("Location:http://$url");
    die;
}

// 加载基础文件
require __DIR__ . '/../thinkphp/base.php';

// 执行应用并响应
Container::get('app')->run()->send();
