<?php
if (file_exists(__DIR__ . '/install.lock')) {
    // 重命名，生成随机名称文件
    rename('./install.php', './' . md5(time()) . '.php');
    exit('已经安装，不可再次安装');
}

$step = $_GET['step'];

?>
<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHCMS安装</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <style>
        .layui-container {
            padding: 0;
        }

        .ptb-5 {
            padding: 50px 0;
        }

        .ptb-1 {
            padding: 10px 0;
        }
    </style>
</head>
<body>
<div class="layui-container">
    <div class="layui-row">
        <nav>
            <ul class="layui-nav">
                <li class="layui-nav-item <?php echo $step ? '' : 'layui-this'; ?>"><a href="/install.php" title="阅读协议">阅读协议</a></li>
                <li class="layui-nav-item <?php echo $step == 1 ? 'layui-this' : ''; ?>"><a href="/install.php?step=1" title="查看配置">查看配置</a></li>
                <li class="layui-nav-item <?php echo $step == 2 ? 'layui-this' : ''; ?>"><a href="/install.php?step=2" title="填写参数">填写参数</a></li>
                <li class="layui-nav-item <?php echo $step == 3 ? 'layui-this' : ''; ?>"><a href="/install.php?step=3" title="安装">安装</a></li>
            </ul>
            <ul class="layui-nav layui-layout-right">
                <li class="layui-nav-item"><a href="" title="帮助" target="_blank">帮助</a></li>
            </ul>
        </nav>
        <?php if (!$step) { ?>
            <main class="ptb-5">
                <fieldset class="layui-elem-field ">
                    <legend>PHCMS使用许可协议</legend>
                    <div class="layui-field-box">
            <pre>
1.前言
重要提示，请仔细阅读：
本协议是您与PHCMS软件（以下简称“软件”）签定的软件许可协议。如果您已安装、复制、下载或以其他任何方式使用该软件，则视为已经接受本协议。如果您不接受本协议的全部或部分条款，您将无权使用本软件。请立即终止安装、拷贝或以其他方式使用该软件，删除您已经安装或保留的该软件的任何组件。

2.软件许可
在您完全接受并遵守本协议的基础上，本协议授予您使用“软件”的某些权利和非独占性许可，允许您依据本协议各项条款许可的用途使用“软件”。

购买"软件"授权码（免费获取，只为更好的服务于产品的升级和改进），请访问：<a href="http://phcms.shenlin.ink" target="_blank">phcms.shenlin.ink</a>

获得软件授权之后，您可以将本“软件”应用于“商业用途”，每份本软件商业授权只能应用于一个域名网站安装，如果您拥有多个使用本软件建设的域名网站，则必须为所拥有的每个网站申请一个授权码。

3.免责说明
本“软件”按“现状”授予许可，您须自行承担使用本“软件”的风险。 我们不对本“软件”提供任何明示、暗示或任何其它形式的担保和表示。在任何情况下，对于因使用或无法使用本软件而导致的任何损失（包括但不仅限于商业利润损失、业务中断或业务信息丢失），无需向您或任何第三方负责。在任何情况下，均不就任何直接的、间接的、附带的、后果性的、特别的、惩戒性的和处罚性的损害赔偿承担任何责任，无论该主张是基于保证、合同、侵权（包括疏忽）或是基于其他原因作出。

本软件可能内置有第三方服务，您应自行评估使用这些第三方服务的风险，由使用此类第三方服务而产生的纠纷，全部责任由您自行承担。

不对使用本“软件”构建的网站中任何信息内容以及导致的任何版权纠纷、法律争议和后果承担任何责任，全部责任由您自行承担。

可能会经常提供“软件”更新或升级，但是没有为根据本协议许可的“软件”提供维护或更新的责任。

4.协议终止
您应理解按授权范围使用许可软件、尊重软件及软件包含内容的知识产权、按规范使用软件、按本协议约定履行义务是您获取我们授权使用软件的前提，如您严重违反本协议，我们将终止使用许可。

您一旦开始复制、下载、安装或者使用本“软件”，即被视为完全理解并接受本协议的各项条款，在享有上述条款授予的许可权力同时，也受到相关的约束和限制，本协议许可范围以外的行为，将直接违反本协议并构成侵权。

一旦您违反本协议的条款，我们随时可能终止本协议、收回许可和授权，并要求您承担相应法律和经济责任。

                                                                                                                            PHCMS开发者
            </pre>
                    </div>
                </fieldset>
                <div class="layui-container">
                    <p style="text-align: center">
                        <a href="?step=1" class="layui-btn layui-btn-normal">同意，进入下一步</a>
                    </p>
                </div>
            </main>
        <?php } elseif ($step == 1) { ?>
            <main class="ptb-5">
                <fieldset class="layui-elem-field ">
                    <legend>查看配置</legend>
                    <div class="layui-field-box">
                        <p class="ptb-1">GD：<?php
                            if (function_exists('gd_info')) {
                                $gd_info = @gd_info();
                                $gd      = $gd_info["GD Version"];
                                echo '支持，版本号：' . $gd;
                            } else {
                                echo '不支持';
                            };
                            ?>
                        </p>
                        <p class="ptb-1">CURL：<?php
                            $funName = 'curl_init';
                            if (!$funName || trim($funName) == '' || preg_match('~[^a-z0-9\_]+~i', $funName, $tmp)) {
                                echo '错误';
                            } elseif (false !== function_exists($funName)) {
                                echo '支持';
                            } else {
                                echo '不支持';
                            }
                            ?>
                        </p>
                        <p class="ptb-1">SMTP：<?php
                            if (get_cfg_var("SMTP")) {
                                echo '支持';
                            } else {
                                echo '不支持';
                            }
                            ?>
                        </p>
                    </div>
                </fieldset>
                <div class="layui-container">
                    <p style="text-align: center">
                        <a href="?step=2" class="layui-btn layui-btn-normal" title="进入下一步">进入下一步</a>
                    </p>
                </div>
            </main>
        <?php } elseif ($step == 2) { ?>
            <main class="ptb-5">
                <fieldset class="layui-elem-field ">
                    <legend>填写参数</legend>
                    <div class="layui-field-box">
                        <form class="layui-form layui-form-pane" action="install.php?step=3" method="post">
                            <div class="layui-form-item">
                                <label class="layui-form-label">数据库地址</label>
                                <div class="layui-input-block">
                                    <input type="text" name="host" value="127.0.0.1" required lay-verify="required"
                                           lay-reqText="请输入数据库地址" placeholder="请输入数据库地址" autocomplete="off"
                                           class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">用户名</label>
                                <div class="layui-input-block">
                                    <input type="text" name="username" required lay-verify="required"
                                           lay-reqText="请输入数据库用户名" placeholder="请输入数据库用户名" autocomplete="off"
                                           class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">密码</label>
                                <div class="layui-input-block">
                                    <input type="text" name="password" required lay-verify="required"
                                           lay-reqText="请输入数据库密码" placeholder="请输入数据库密码" autocomplete="off"
                                           class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">数据库名</label>
                                <div class="layui-input-block">
                                    <input type="text" name="name" required lay-verify="required" lay-reqText="请输入数据库名"
                                           placeholder="请输入数据库名" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">端口</label>
                                <div class="layui-input-block">
                                    <input type="text" name="port" value="3306" required lay-verify="required|number"
                                           lay-reqText="请输入端口"
                                           placeholder="请输入端口，默认3306" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">管理员账号</label>
                                <div class="layui-input-block">
                                    <input type="text" name="admin_name" required lay-verify="required"
                                           lay-reqText="请输入管理员账号" placeholder="请输入管理员账号" autocomplete="off"
                                           class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">管理员密码</label>
                                <div class="layui-input-block">
                                    <input type="text" name="admin_pass" required lay-verify="required"
                                           lay-reqText="请输入管理员密码" placeholder="请输入管理员密码" autocomplete="off"
                                           class="layui-input">
                                </div>
                            </div>
                            <div class="layui-container">
                                <p style="text-align: center">
                                    <button class="layui-btn layui-btn-normal">安装</button>
                                </p>
                            </div>
                        </form>
                    </div>
                </fieldset>
            </main>
        <?php } elseif ($step == 3) { ?>
            <?php
            @$conn = new mysqli($_POST['host'], $_POST['username'], $_POST['password']);
            if ($conn->connect_error) {
                die('数据库连接失败：' . $conn->connect_errno);
            }
            $name = $_POST['name'];
            // 判断数据库是否存在
            $findSql = "SELECT * FROM information_schema.SCHEMATA WHERE SCHEMA_NAME = $name";
            $res     = $conn->query($findSql);
            if (!$res) {
                // 创建数据库
                $conn->query("CREATE DATABASE $name;");
            }
            // 选择数据库
            mysqli_select_db($conn, $name);
            // 设置字符集
            $conn->set_charset('utf-8mb4');
            $f = fopen('../sql/shenlin.sql', 'r') or die('未找到数据库文件');
            $sqlData = fread($f, filesize('../sql/shenlin.sql'));
            $sql     = explode(';', $sqlData);
            foreach ($sql as $k => $v) {
                // 执行SQL语句
                $result = $conn->query($v);
            }
            fclose($f);
            // 创建初始账号和密码
            $initialSql    = "INSERT INTO admin(create_time,update_time,name,pass,type,status) VALUES ('" . time() . "','" . time() . "','" . $_POST['admin_name'] . "','" . md5($_POST['admin_pass'] . 'phCms') . "',1,1)";
            $initialResult = $conn->query($initialSql);
            if ($initialResult) {
                file_put_contents('../config/install.txt', '初始账号：' . $_POST['admin_name'] . '，初始密码：' . $_POST['admin_pass'] . '，安装时间：' . date('Y-m-d H:i:s', time()));
            } else {
                exit('初始账户创建失败');
            }
            $conn->close();
            // 修改 config/database.php配置文件
            $databaseData = file_get_contents('../config/database.php');
            // 服务器地址
            $databaseData = str_replace('phcms_hostname', $_POST['host'], $databaseData);
            // 数据库名
            $databaseData = str_replace('phcms_database', $_POST['name'], $databaseData);
            // 用户名
            $databaseData = str_replace('phcms_username', $_POST['username'], $databaseData);
            // 密码
            $databaseData = str_replace('phcms_password', $_POST['password'], $databaseData);
            // 端口
            $databaseData = str_replace('phcms_hostport', $_POST['port'], $databaseData);
            // 写入新配置信息
            file_put_contents('../config/database.php', $databaseData);
            // 新建空白文件
            touch('./install.lock');
            ?>
            <main class="ptb-5">
                <fieldset class="layui-elem-field ">
                    <legend>安装完成</legend>
                    <div class="layui-field-box">
                        <p>初始账号、密码已经保存至config/install.txt文件中</p>
                    </div>
                </fieldset>
                <div class="layui-container">
                    <p style="text-align: center">
                        <a href="/" class="layui-btn layui-btn-normal" title="查看前台">查看前台</a>
                        <a href="/defaultPath" class="layui-btn layui-btn-normal" title="登录后台">登录后台</a>
                    </p>
                </div>
            </main>
        <?php } ?>
    </div>
</div>
<script src="/layui/layui.js"></script>
</body>
</html>
