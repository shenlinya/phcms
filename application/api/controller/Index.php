<?php

namespace app\api\controller;

use app\common\model\Content;
use app\common\model\System;
use app\common\model\Tag;
use think\Controller;

class Index extends Controller
{
    /**
     * 定时发文
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function index()
    {
        $data = Content::where(['status' => 0])->whereTime('create_time', 'today')->update(['status' => 1]);
        if ($data) {
            echo date('Y-m-d', time()) . '，成功发文，共计：' . $data . '条';
        } else {
            echo '无内容发布';
        }
    }

    /**
     * 百度站长普通收录链接提交
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function push()
    {
        // 获取基础参数信息
        $system = System::where(['id' => 1])->field('host,site,token')->find();
        if (!$system['site'] or !$system['token']) {
            echo '请先配置参数信息';
            die;
        }
        // 获取文章内容链接
        $link = Content::where(['status' => 1])->whereTime('create_time', 'today')->field('link')->select();
        $url  = [];
        foreach ($link as $k => $v) {
            $url[] = $system['host'] . $v['link'];
        }
        // 获取标签内容链接
        $tag = Tag::whereTime('create_time', 'today')->field('id')->select();
        foreach ($tag as $k => $v) {
            $url[] = $system['host'] . '/tag-' . $v['id'] . '/';
        }
        // 推送链接数量
        $count = count($url);
        if ($count < 1) {
            echo '暂无链接推送';
            die;
        }
        // curl 推送代码
        $site    = $system['site'];
        $token   = $system['token'];
        $api     = 'http://data.zz.baidu.com/urls?site=' . $site . '&token=' . $token;
        $ch      = curl_init();
        $options = array(
            CURLOPT_URL            => $api,
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => implode("\n", $url),
            CURLOPT_HTTPHEADER     => array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        if (isset($result['success']) and $result['success'] > 0) {
            echo '推送成功，共计' . $count . '条';
        } else {
            echo $this->seoErrorMessage($result['error'], $result['message']);
        }
    }

    /**
     * 百度站长提交接口信息反馈
     * @param $code
     * @param $message
     * @return string
     */
    private function seoErrorMessage($code, $message)
    {
        $data = [
            400 => [
                'site error'                      => '站点未在站长平台验证',
                'empty content'                   => 'post内容为空',
                'only 2000 urls are allowed once' => '每次最多只能提交2000条链接',
                'over quota'                      => '超过每日配额了，超配额后再提交都是无效的',
            ],
            401 => [
                'token is not valid' => 'token错误'
            ],
            404 => [
                'not found' => '接口地址填写错误'
            ],
            500 => [
                'internal error, please try later' => '服务器偶然异常，通常重试就会成功'
            ]
        ];
        return $data[$code][$message];
    }
}
