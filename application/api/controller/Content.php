<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/21 0021 13:58
 */

namespace app\api\controller;


use think\App;
use think\Controller;

class Content extends Controller
{
    protected $content_model;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->content_model = new \app\common\model\Content();
    }

    /**
     * 最新资讯
     * @return void
     * @author shenlin
     * @time 2021/10/21 0021 14:00
     * @phone 13614048679
     */
    public function index()
    {
        $limit = input('get.limit', 10);
        $data  = $this->content_model->getAll(['status' => 1], '*', 'id desc', $limit);
        $this->result(['data' => $data], 200);
    }
}
