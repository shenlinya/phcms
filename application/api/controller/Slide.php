<?php

/**
 * 轮播图
 * @author     shenlin
 * @ctime:     2021/10/21 0021 13:55
 */

namespace app\api\controller;


use think\App;
use think\Controller;

class Slide extends Controller
{
    protected $slide_model;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->slide_model = new \app\common\model\Slide();
    }

    /**
     * 轮播图接口
     * @return void
     * @author shenlin
     * @time 2021/10/21 0021 13:56
     * @phone 13614048679
     */
    public function index()
    {
        $data = $this->slide_model->getAll(['status' => 1], '*', 'num asc');
        $this->result(['data' => $data], 200);
    }
}
