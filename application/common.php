<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 生成密码规则
 * @param $value
 * @return string
 */
function getPassRule($value)
{
    return md5($value . 'phCms');
}

/**
 * 操作结果返回
 * @param $res
 * @param $jumpAddress
 * @return \think\response\Json
 */
function operationResult($res, $jumpAddress = '')
{
    if ($res) {
        return json(['code' => 200, 'message' => '操作成功', 'jumpAddress' => $jumpAddress]);
    } else {
        return json(['code' => 100, 'message' => '操作失败']);
    }
}


/**
 * 运营管理 - 账户状态转换
 * @param $v
 * @return mixed
 */
function changeAdminStatus($v)
{
    $data = ['冻结', '正常'];
    return $data[$v];
}

/**
 * 运营管理 - 账户类型转换
 * @param $v
 * @return mixed
 */
function changeAdminType($v)
{
    $data = ['普通', '管理员'];
    return $data[$v];
}

/**
 * 友情链接状态转换
 * @param $v
 * @return mixed
 */
function changLinkStatus($v)
{
    $data = ['', '显示', '隐藏'];
    return $data[$v];
}

/**
 * 导航管理状态转换
 * @param $v
 * @return mixed
 */
function changNavStatus($v)
{
    $data = ['隐藏', '显示'];
    return $data[$v];
}

/**
 * 导航管理类型转换
 * @param $v
 * @return string
 */
function changeNavType($v)
{
    $data = ['栏目', '单页', '留言', '链接'];
    return $data[$v];
}

/**
 * 留言管理状态转换
 * @param $v
 * @return string
 */
function changMessageStatus($v)
{
    $data = ['未读', '已读'];
    return $data[$v];
}

/**
 * 生成静态文件
 * @param $content     string 内容
 * @param $filePath    string 文件保存路径
 * @return bool
 */
function buildHtml($content, $filePath = '/')
{
    $filePath = '.' . $filePath . '.' . config('url_html_suffix');
    if (!file_exists($filePath)) {
        //判断目录
        if (!is_dir(dirname($filePath))) {
            mkdir(dirname($filePath), 0755, true);
        }
    }
    file_put_contents($filePath, $content);
    return true;
}


/**
 * 数据类型转换
 * @param $data
 * @return array
 * @author shenlin
 * @time 2021/8/2 0002 16:05
 * @phone 13614048679
 */
function getTree($data)
{
    $items = [];
    // 构建一个新的数组 新数组的key值是自己的主键id值
    // 为何要构建这样一个数组 这就是和下面第二个foreach有关了
    foreach ($data as $v) {
        $items[$v['id']] = $v;
    }
    $tree = [];
    // 将数据进行无限极分类
    foreach ($items as $key => $val) {
        // 关键是看这个判断,是顶级分类就给$tree,不是的话继续拼凑子分类（结合上述&用法）
        if (isset($items[$val['parent']])) {
            $items[$val['parent']]['children'][] = &$items[$key];
        } else {
            $tree[] = &$items[$key];
        }
    }
    // 返回无限极分类后的数据
    return $tree;
}
