<?php

/**
 *
 * @author     shenlin
 * @ctime:     2022/2/17 0017 9:29
 */

namespace app\index\controller;


use think\App;

class Content extends Base
{
    protected $content_model;
    protected $nav_model;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->content_model = new \app\common\model\Content();
        $this->nav_model     = new \app\common\model\Nav();
    }

    /**
     * 新闻列表
     * @param $id
     * @return \think\response\View
     * @author shenlin
     * @time 2022/2/17 0017 11:25
     * @phone 13614048679
     */
    public function index($id)
    {
        if (!$id) {
            abort(404);
        }
        $data = $this->content_model->getOne(['id' => $id, 'status' => 1], '*', 'id DESC');
        if (!$data) {
            abort(404);
        }
        $info = $this->nav_model->getOne(['id' => $data['type']], 'id,title', 'id DESC');
        // 阅读量自增
        $this->content_model->dataIns($id, 'views');
        // 上下页
        $lastData = $this->getPageInfo([
            ['type', '=', $data['type']],
            ['status', '=', 1],
            ['id', '<', $id]
        ], 'id,title', 'id desc');
        $nextData = $this->getPageInfo([
            ['type', '=', $data['type']],
            ['status', '=', 1],
            ['id', '>', $id]
        ], 'id,title', 'id asc');
        // 文章标签解析
        $tag = $this->changeTag($data['tag'], $data['tag_id']);
        // 相关新闻
        $aboutWhere[] = ['status', '=', 1];
        $aboutWhere[] = ['type', '=', $data['type']];
        $aboutWhere[] = ['id', '<>', $id];
        $aboutContent = $this->content_model->getAll($aboutWhere, 'id,create_time,title,cover,views', 6);
        return view('/info',
            [
                'data'            => $data,
                'seo_title'       => $data['title'],
                'seo_keywords'    => $data['keywords'],
                'seo_description' => $data['represent'],
                'info'            => $info,
                'lastData'        => $lastData,
                'nextData'        => $nextData,
                'tag'             => $tag,
                'aboutContent'    => $aboutContent
            ]
        );
    }


    /**
     * 获取上下页数据
     * @param $where
     * @param $field
     * @param $order
     * @return array|\PDOStatement|string|\think\Model|null
     * @author shenlin
     * @time 2022/2/17 0017 9:43
     * @phone 13614048679
     */
    private function getPageInfo($where, $field, $order)
    {
        return $this->content_model->getOne($where, $field, $order);
    }

    /**
     * 标签转换
     * @param $tag
     * @param $tagId
     * @return array
     * @author shenlin
     * @time 2022/2/17 0017 9:43
     * @phone 13614048679
     */
    private function changeTag($tag, $tagId)
    {
        $tag   = array_filter(explode('，', $tag));
        $tagId = array_filter(explode('，', $tagId));
        $data  = [];
        foreach ($tag as $k => $v) {
            $data[$k]['title'] = $v;
            $data[$k]['id']    = $tagId[$k];
        }
        return $data;
    }

    /**
     * 搜索
     * @return \think\response\View
     * @author shenlin
     * @time 2022/2/17 0017 13:06
     * @phone 13614048679
     */
    public function search()
    {
        $page = input('get.page');
        $name = input('get.q');
        if (!$name) {
            abort(404);
        }
        $where[]           = ['status', '=', 1];
        $where[]           = ['title', 'like', "%" . $name . "%"];
        $data              = $this->content_model->getSearchList($where, 'id,create_time,title,cover,represent,views', 'id DESC');
        $pageHtml          = $data->render();
        $data              = $data->items();
        $info['cat_title'] = $title = $name . '搜索';
        $page - 1 && $title .= '第' . $page . '页';
        return view('/list',
            [
                'seo_title'       => $title,
                'seo_keywords'    => '',
                'seo_description' => '',
                'info'            => $info,
                'url'             => '',
                'data'            => $data,
                'page'            => $pageHtml
            ]
        );
    }
}
