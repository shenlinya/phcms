<?php

/**
 * 首页
 * @author     shenlin
 * @ctime:     2021/10/22 0022 11:15
 */

namespace app\index\controller;

class Index extends Base
{
    public function index()
    {
        return view('/index',
            [
                'seo_title'       => $this->systemData['title'],
                'seo_keywords'    => $this->systemData['keywords'],
                'seo_description' => $this->systemData['represent'],
            ]
        );
    }
}
