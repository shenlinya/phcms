<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/27 0027 13:21
 */

namespace app\index\controller;


use app\common\model\Content;
use app\common\model\Tag;
use think\App;

class Nav extends Base
{
    protected $nav_model;
    protected $content_model;
    protected $tag_model;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->nav_model     = new \app\common\model\Nav();
        $this->content_model = new Content();
        $this->tag_model     = new Tag();
    }

    /**
     * 列表页
     * @param $id
     * @param $page
     * @return \think\response\View
     * @author shenlin
     * @time 2021/10/28 0028 10:46
     * @phone 13614048679
     */
    public function index($id, $page)
    {
        $info = $this->nav_model->getOne(['id' => $id], '*', 'id ASC');
        if (!$info) {
            abort(404);
        }
        $data     = $this->content_model->getIndexList('/list/' . $id . '-[PAGE].html', ['type' => $id, 'status' => 1], 'id,create_time,title,cover,represent,views', 'id DESC');
        $pageHtml = $data->render();
        $data     = $data->items();
        $title    = $info['title'];
        $page - 1 && $title .= '第' . $page . '页';
        return view('/list',
            [
                'seo_title'       => $title,
                'seo_keywords'    => $info['keywords'],
                'seo_description' => $info['represent'],
                'info'            => $info,
                'url'             => '/list/' . $id . '-' . $page . '.html',
                'data'            => $data,
                'page'            => $pageHtml
            ]
        );
    }

    /**
     * 列表页
     * @param $id
     * @param $page
     * @return \think\response\View
     * @author shenlin
     * @time 2021/10/28 0028 10:46
     * @phone 13614048679
     */
    public function tag($id, $page)
    {
        $info = $this->tag_model->getOne(['id' => $id], 'title', 'id ASC');
        if (!$info) {
            abort(404);
        }
        $where[]           = ['status', '=', 1];
        $where[]           = ['tag_id', 'like', "%，" . $id . "，%"];
        $data              = $this->content_model->getIndexList('/tag/' . $id . '-[PAGE].html', $where, 'id,create_time,title,cover,represent,views', 'id DESC');
        $pageHtml          = $data->render();
        $data              = $data->items();
        $title             = $info['title'];
        $info['cat_title'] = $info['title'];
        $page - 1 && $title .= '第' . $page . '页';
        return view('/list',
            [
                'seo_title'       => $title . '标签',
                'seo_keywords'    => '',
                'seo_description' => '',
                'info'            => $info,
                'url'             => '/tag/' . $id . '-' . $page . '.html',
                'data'            => $data,
                'page'            => $pageHtml
            ]
        );
    }
}
