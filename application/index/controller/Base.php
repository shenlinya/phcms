<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/22 0022 11:15
 */

namespace app\index\controller;

use app\common\model\System;
use think\App;
use think\Controller;

class Base extends Controller
{
    protected $system_model;
    protected $systemData;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->system_model = new System();
        $this->systemData   = cache('system');
        if (!$this->systemData) {
            $this->systemData = $this->system_model->getOne(['id' => 1], '*');
            if (!$this->systemData) {
                exit('请前往后台配置站点参数');
            }
            cache('system', $this->systemData);
        }
        $this->assign('system', $this->systemData);
        $this->assign('bindingType', cache('bindingType'));
        $this->assign('bindingContent', cache('bindingContent'));
    }
}
