<?php

/**
 * 留言 意见反馈
 * @author     shenlin
 * @ctime:     2021/10/27 0027 13:19
 */

namespace app\index\controller;

use think\App;

class Message extends Base
{
    protected $nav_model;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->nav_model = new \app\common\model\Nav();
    }

    /**
     * 留言 意见反馈
     * @return \think\response\View
     * @author shenlin
     * @time 2021/10/27 0027 13:20
     * @phone 13614048679
     */
    public function index()
    {
        $info = $this->nav_model->getOne(['link' => '/message.html'], 'title,keywords,represent,cat_title', 'id DESC');
        if (!$info) {
            exit('无法找到栏目信息');
        }
        return view('/message',
            [
                'seo_title'       => $info['title'],
                'seo_keywords'    => $info['keywords'],
                'seo_description' => $info['represent'],
            ]
        );
    }

    public function data()
    {

    }
}
