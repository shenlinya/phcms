<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/27 0027 13:20
 */

namespace app\index\controller;


use think\App;

class Page extends Base
{
    protected $page_model;
    protected $nav_model;

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->page_model = new \app\common\model\Page();
        $this->nav_model  = new \app\common\model\Nav();
    }

    /**
     * 单页面
     * @param $id
     * @return \think\response\View
     * @author shenlin
     * @time 2021/10/27 0027 13:35
     * @phone 13614048679
     */
    public function index($id)
    {
        if (!$id) {
            abort(404);
        }
        $data = $this->page_model->getOne(['id' => $id], '*', 'id ASC');
        if (!$data) {
            abort(404);
        }
        $info = $this->nav_model->getOne(['id' => $data['nav_id']], '*', 'id ASC');
        return view('/page',
            [
                'data'            => $data,
                'info'            => $info,
                'seo_title'       => $info['title'],
                'seo_keywords'    => $info['keywords'],
                'seo_description' => $info['represent'],
                'url'             => '/page/' . $id . '.html'
            ]
        );
    }
}
