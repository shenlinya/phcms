<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/22 0022 13:43
 */
return [
    // 预先加载的标签库
    'taglib_pre_load' => 'app\common\taglib\Link,app\common\taglib\Tag,app\common\taglib\Nav,app\common\taglib\Slide,app\common\taglib\Content,app\common\taglib\Page',
    'view_path'       => str_replace('config', 'view/' . env('TP_PATH') . '/', __DIR__)
];
