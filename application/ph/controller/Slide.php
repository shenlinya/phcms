<?php

/**
 * 轮播图管理
 */

namespace app\ph\controller;

class Slide extends Base
{
    protected $slide_model;

    public function __construct()
    {
        parent::__construct();
        $this->slide_model = new \app\common\model\Slide();
    }

    /**
     * 列表功能
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $data = $this->slide_model->getList([]);
        $page = $data->render();
        $data = $data->items();
        return view('slide/index',
            [
                'data' => $data,
                'page' => $page
            ]
        );
    }

    /**
     * 添加
     * @return \think\response\View
     */
    public function data_insert()
    {
        return view('slide/data_insert');
    }

    /**
     * 修改
     * @param $id
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function data_update($id)
    {
        if (!$id) {
            $this->error('缺失参数ID');
        }
        $data = $this->slide_model->getOne(['id' => $id], '*', 'id desc');
        if (!$data) {
            $this->error('获取信息失败');
        }
        return view('slide/data_update',
            [
                'data' => $data
            ]
        );
    }

    /**
     * 数据操作
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function data_save()
    {
        $data['num']     = input('post.num');
        $data['link']    = input('post.link');
        $data['path']    = input('post.path');
        $data['content'] = input('post.content');
        $id              = input('post.id');

        $validate = new \app\common\validate\Slide();
        if (!$validate->check($data)) {
            return json(['code' => 100, 'message' => $validate->getError()]);
        }

        if ($id) {
            $res = $this->slide_model->editData(['id' => $id], $data);
        } else {
            $res = $this->slide_model->addData($data);
        }
        return operationResult($res, url('Slide/index'));
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function data_delete()
    {
        $id = input('post.id');
        if (!$id) {
            return json(['code' => 100, 'message' => '缺失ID参数']);
        }
        $res = $this->slide_model->dataDelete($id);
        return operationResult($res);
    }
}
