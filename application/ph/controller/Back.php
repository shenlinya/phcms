<?php

/**
 *
 * @author     shenlin
 * @ctime:     2022/2/21 0021 15:48
 */

namespace app\ph\controller;

use Ifsnop\Mysqldump as sqlBack;
use think\Db;
use think\facade\Env;


class Back extends Base
{

    /**
     * 表数据
     * @return \think\response\View
     * @author shenlin
     * @time 2022/2/21 0021 16:23
     * @phone 13614048679
     */
    public function index()
    {
        // 获取数据库信息
        $name = config('database.database');
        $sql  = "SELECT TABLE_NAME AS 'name',
 TABLE_ROWS AS'limit',
 TABLE_COMMENT AS 'comment',
 CREATE_TIME AS 'create_time',
 ENGINE AS 'type',
 TABLE_COLLATION AS 'collation',
 truncate(DATA_LENGTH/1024/1024, 2) AS 'db_size',
 truncate(DATA_FREE/1024/1024, 2) AS 'db_free',
 truncate(INDEX_LENGTH/1024/1024, 2) AS 'index_size'
 FROM information_schema.tables
 WHERE table_schema='$name'
 ORDER BY data_length DESC ,
 index_length DESC ;";
        $data = Db::query($sql);
        return view('back/index',
            [
                'data' => $data
            ]
        );
    }

    /**
     * 表字段
     * @param $name
     * @return \think\response\View
     * @author shenlin
     * @time 2022/2/21 0021 16:51
     * @phone 13614048679
     */
    public function info($name)
    {
        if (!$name) {
            abort(404);
        }
        $database = config('database.database');
        $sql      = "SELECT COLUMN_NAME AS c_name,
 COLUMN_COMMENT AS c_comment,
 COLUMN_TYPE AS c_type,
 COLUMN_KEY AS c_key
 FROM information_schema.columns
 WHERE table_schema = '$database'
 AND TABLE_NAME = '$name'";
        $data     = Db::query($sql);
        return view('back/info',
            [
                'data' => $data
            ]
        );
    }

    /**
     * 创建表语句
     * @param $name
     * @return \think\response\View
     * @author shenlin
     * @time 2022/2/21 0021 17:24
     * @phone 13614048679
     */
    public function sql($name)
    {
        $this->view->engine->layout(false);
        if (!$name) {
            abort(404);
        }
        $sql  = "SHOW CREATE TABLE $name";
        $data = Db::query($sql);
        return view('back/sql',
            [
                'data' => $data[0]['Create Table']
            ]
        );
    }

    /**
     * 表行数
     * @param $tableName
     * @return mixed
     * @author shenlin
     * @time 2022/2/21 0021 16:30
     * @phone 13614048679
     */
    private function getTableCountLine($tableName)
    {
        $data = Db::query('SELECT count(id) FROM ' . $tableName);
        return $data[0]['count(id)'];
    }

    /**
     * 备份文件列表
     * @param string $type
     * @return \think\response\View
     * @author shenlin
     * @time 2022/2/22 0022 8:38
     * @phone 13614048679
     */
    public function file($type = 'database')
    {
        $path = [
            'database' => '../backup/database/',
            'site'     => '../backup/site/',
            'static'   => '../backup/static/'
        ];
        $data = scandir($path[$type]);
        unset($data[0], $data[1]);
        return view('back/file',
            [
                'data' => $data,
                'type' => $type
            ]
        );
    }

    /**
     * mysql 链接
     * @return sqlBack\Mysqldump
     * @author shenlin
     * @time 2022/2/21 0021 16:01
     * @phone 13614048679
     */
    public function contact()
    {
        $savePath = '../backup/database/' . date('Y-m-d_H-i-s') . '.sql';
        $host     = config('database.hostname'); // 数据库链接地址
        $name     = config('database.database'); // 数据库名称
        $username = config('database.username'); // 数据库用户名
        $password = config('database.password'); // 数据库密码
        try {
            $sql = new sqlBack\Mysqldump('mysql:host=' . $host . ';dbname=' . $name, $username, $password);
            $sql->start($savePath);
            $this->success('备份成功');
        } catch (\Exception $e) {
            if (!$e->getMessage()) {
                $this->success('备份成功');
            } else {
                $this->error($e->getMessage());
            }
        }
    }

    /**
     * 数据库文件删除
     * @param $name
     * @return void
     * @author shenlin
     * @time 2022/2/22 0022 9:25
     * @phone 13614048679
     */
    public function delete($name)
    {
        if (!$name) abort(404);
        $path       = '../backup/database/' . $name;
        $fileStatus = file_exists($path);
        if (!$fileStatus) {
            $this->error('文件不存在');
        }
        unlink($path);
        $this->success('删除成功');
    }

    /**
     * 文件下载
     * @param $name
     * @return \think\response\Download
     * @author shenlin
     * @time 2022/2/22 0022 9:28
     * @phone 13614048679
     */
    public function download($name)
    {
        if (!$name) abort(404);
        $path       = '../backup/database/' . $name;
        $fileStatus = file_exists($path);
        if (!$fileStatus) {
            $this->error('文件不存在');
        }
        return download($path, time() . '.sql');
    }
}
