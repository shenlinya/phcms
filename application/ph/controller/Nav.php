<?php

/**
 * 导航管理
 */

namespace app\ph\controller;

class Nav extends Base
{
    protected $nav_model;

    public function __construct()
    {
        parent::__construct();
        $this->nav_model = new \app\common\model\Nav();
    }

    /**
     * 列表功能
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $data = $this->nav_model->getAll([], '*');
        $data = getTree($data->toArray());
        return view('nav/index',
            [
                'data' => $data
            ]
        );
    }

    /**
     * 添加
     * @param $parent
     * @return \think\response\View
     * @author shenlin
     * @time 2021/8/26 0026 16:16
     * @phone 13614048679
     */
    public function data_insert($parent = 0)
    {
        $catInfo = '无';
        if ($parent) {
            $catInfo = $this->nav_model->getValue(['id' => $parent], 'title');
        }
        return view('nav/data_insert',
            [
                'catInfo' => $catInfo,
                'parent'  => $parent
            ]
        );
    }

    /**
     * 修改
     * @param $id
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function data_update($id)
    {
        if (!$id) {
            $this->error('缺失参数ID');
        }
        $data = $this->nav_model->getOne(['id' => $id], '*', 'id desc');
        if (!$data) {
            $this->error('获取信息失败');
        }
        $cat = $this->nav_model->getAll(['parent' => 0], 'id,title');
        if ($data['type'] === 3) {
            $data['template'] = $data['link'];
        }
        return view('nav/data_update',
            [
                'data' => $data,
                'cat'  => $cat
            ]
        );
    }

    /**
     * 数据操作
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function data_save()
    {
        $data['num']       = input('post.num');
        $data['parent']    = input('post.parent');
        $data['cat_title'] = input('post.cat_title');
        $data['title']     = input('post.title');
        $data['keywords']  = input('post.keywords');
        $data['represent'] = input('post.represent');
        $data['status']    = input('post.status');
        $data['type']      = input('post.type');
        $data['template']  = input('post.template');
        $data['link']      = input('post.link');
        $data['id']        = input('post.id');
        if ($data['type'] === '3') {
            $data['link']     = $data['template'];
            $data['template'] = '';
        }
        $validate = new \app\common\validate\Nav;
        if (!$validate->check($data)) {
            return json(['code' => 100, 'message' => $validate->getError()]);
        }
        if ($data['id']) {
            $res = $this->nav_model->editData(['id' => $data['id']], $data);
        } else {
            $data['create_time'] = time();
            $res                 = $data['id'] = \app\common\model\Nav::insertGetId($data);
        }
        $this->nav_model->editData(['id' => $data['id']], ['link' => $this->getLink($data['id'], $data['type'], $data['link'])]);
        return operationResult($res, url('Nav/index'));
    }

    /**
     * 链接规则
     * @param $id
     * @param $type
     * @param $link
     * @return string
     * @author shenlin
     * @time 2021/10/25 0025 11:11
     * @phone 13614048679
     */
    public function getLink($id, $type, $link = '')
    {
        switch ($type) {
            case 0:
                // 列表页
                return '/list/' . $id . '-1.html';
                break;
            case 1:
                // 单页面
                return '/page/' . $id . '.html';
                break;
            case 2:
                // 留言
                return '/message.html';
                break;
            case 3:
                // 链接
                return $link;
                break;
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function data_delete()
    {
        $id = input('post.id');
        if (!$id) {
            return json(['code' => 100, 'message' => '缺失ID参数']);
        }
        $res = $this->nav_model->dataDelete($id);
        return operationResult($res);
    }

    /**
     * 清除缓存
     */
    public function data_clear()
    {
        cache("nav", null);
        $this->success("清除成功");
    }

    /**
     * 获取子集分类数据
     * @return void
     * @author shenlin
     * @time 2021/9/6 0006 15:18
     * @phone 13614048679
     */
    public function data()
    {
        $id   = input('post.id');
        $data = $this->nav_model->getAll(['parent' => $id], 'id,cat_title');
        $this->result($data, 200);
    }
}
