<?php

/**
 * 公共控制器
 */

namespace app\ph\controller;

use think\Controller;

class Base extends Controller
{
    protected $userId;
    protected $userName;
    protected $userType;

    /**
     * session验证
     * Base constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userId   = session('userId', '', 'phCms');
        $this->userName = session('userName', '', 'phCms');
        $this->userType = session('userType', '', 'phCms');
        if (!$this->userId) {
            //登录提示页
            $this->redirect('login/tip');
        } else {
            $this->assign('userName', $this->userName);
            $this->assign('userId', $this->userId);
            $this->assign('userType', $this->userType);
        }
        $empty = '<div id="notFound"><i class="layui-icon layui-icon-tips"></i>暂无数据</div><style>.layui-table{display: none}</style>';
        $this->assign('empty', $empty);
        $this->assign('app_host', config("app.app_host"));
        // 调试模式
        $this->assign('beBugStatus', config('app.app_debug'));
    }
}
