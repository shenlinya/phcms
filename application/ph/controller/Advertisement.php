<?php

/**
 * 广告管理
 */

namespace app\ph\controller;

class Advertisement extends Base
{
    protected $advertisement_model;

    public function __construct()
    {
        parent::__construct();
        $this->advertisement_model = new \app\common\model\Advertisement();
    }

    /**
     * 列表
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $data = $this->advertisement_model->getList([]);
        $page = $data->render();
        $data = $data->items();
        return view('advertisement/index', ['data' => $data, 'page' => $page]);
    }

    /**
     * 添加 视图
     * @return \think\response\View
     */
    public function data_insert()
    {
        return view('advertisement/data_insert');
    }

    /**
     * 修改
     * @param $id
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function data_update($id)
    {
        if (!$id) {
            $this->error('缺失参数');
        }
        $data = $this->advertisement_model->getOne(['id' => $id], '*', 'id desc');
        if (!$data) {
            $this->error('数据获取失败');
        }
        return view('advertisement/data_update', ['data' => $data]);
    }

    /**
     * 添加/修改 操作
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function data_save()
    {
        $data['title']   = input('post.title');
        $data['content'] = input('post.content');
        $data['num']     = input('post.num');
        $data['link']    = input('post.link');
        $data['path']    = input('post.path');
        $id              = input('post.id');

        $validate = new \app\common\validate\Advertisement;
        if (!$validate->check($data)) {
            return json(['code' => 100, 'message' => $validate->getError()]);
        }

        if ($id) {
            $res = $this->advertisement_model->editData(['id' => $id], $data);
        } else {
            $res = $this->advertisement_model->addData($data);
        }
        return operationResult($res, url("Advertisement/index"));
    }


    /**
     * 删除
     * @return \think\response\Json
     */
    public function data_delete()
    {
        $id = input('post.id');
        if (!$id) {
            return json(['code' => 100, 'msg' => '缺少参数ID']);
        }
        $res = $this->advertisement_model->dataDelete($id);
        return operationResult($res);
    }

    /**
     * 清除缓存
     */
    public function data_clear()
    {
        cache("advertisement", null);
        $this->success("清除成功");
    }
}
