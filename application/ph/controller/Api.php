<?php

/**
 * 公共接口管理
 */

namespace app\ph\controller;

use think\Controller;
use think\captcha\Captcha;

class Api extends Controller
{
    /**
     * 图形验证码
     * @return \think\Response
     */
    public function image()
    {
        $config = [
            'fontSize' => 30,
            'length' => 4,
            'useCurve' => false,
            'useNoise' => false,
            'codeSet' => '023456789'
        ];
        $captcha = new Captcha($config);

        return $captcha->entry();
    }

    /**
     * 图片上传
     * @return \think\response\Json
     */
    public function upload()
    {
        $file = request()->file('image');
        // 移动到框架应用根目录/uploads/image/ 目录下
        $info = $file->validate(['size' => 2097152, 'ext' => 'jpeg,jpg,png,gif'])->move('./uploads/image/');
        if ($info) {
            // 成功上传后 获取上传信息
            $imgInfo = '/uploads/image/' . date('Ymd', time()) . "/" . $info->getFilename();
            return json(['code' => 200, 'message' => '上传成功', 'data' => $imgInfo,]);
        } else {
            // 上传失败获取错误信息
            return json(['code' => 100, 'message' => $file->getError()]);
        }
    }

    /**
     * sitemap文件上传
     * @return \think\response\Json
     */
    public function map()
    {
        $file = request()->file('map');
        // 移动到框架应用根目录/sitemap/ 目录下
        $info = $file->validate(['size' => 10485760, 'ext' => 'xml,html,txt'])->move('./sitemap/', '', false);
        if ($info) {
            // 成功上传后 获取上传信息
            $imgInfo = '/sitemap/' . $info->getFilename();
            return json(['code' => 200, 'message' => '上传成功', 'data' => $imgInfo,]);
        } else {
            // 上传失败获取错误信息
            return json(['code' => 100, 'message' => $file->getError()]);
        }
    }
}
