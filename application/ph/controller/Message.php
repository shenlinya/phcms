<?php

/**
 * 留言管理
 */
namespace app\ph\controller;

class Message extends Base
{
    protected $message_model;

    public function __construct()
    {
        parent::__construct();
        $this->message_model = new \app\common\model\Message();
    }

    /**
     * 列表
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $data = $this->message_model->getList([]);
        $page = $data->render();
        $data = $data->items();
        return view('message/index', ['data' => $data, 'page' => $page]);
    }

    /**
     * 修改
     * @param $id
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function data_update($id)
    {
        if (!$id) {
            $this->error('缺失参数');
        }
        $data = $this->message_model->getOne(['id' => $id], '*', 'id desc');
        if (!$data) {
            $this->error('数据获取失败');
        }
        return view('message/data_update', ['data' => $data]);
    }

    /**
     * 添加/修改 操作
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function data_save()
    {
        $data['status']      = 1;
        $data['update_time'] = time();
        $data['reply']       = input('post.reply');
        $id                  = input('post.id');
        $res                 = $this->message_model->editData(['id' => $id], $data);
        return operationResult($res, url("Message/index"));
    }


    /**
     * 删除
     * @return \think\response\Json
     */
    public function data_delete()
    {
        $id = input('post.id');
        if (!$id) {
            return json(['code' => 100, 'msg' => '缺少参数ID']);
        }
        $res = $this->message_model->dataDelete($id);
        return operationResult($res);
    }
}
