<?php

/**
 * 标签管理
 */

namespace app\ph\controller;

class Tag extends Base
{
    protected $tag_model;

    public function __construct()
    {
        parent::__construct();
        $this->tag_model = new \app\common\model\Tag();
    }

    /**
     * 列表
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $where = [];
        $title = input('get.title');
        $title && $where[] = ['title', '=', $title];
        $data = $this->tag_model->getList($where);
        $page = $data->render();
        $data = $data->items();
        return view('tag/index', [
            'data'  => $data,
            'page'  => $page,
            'title' => $title
        ]);
    }

    /**
     * 添加 视图
     * @return \think\response\View
     */
    public function data_insert()
    {
        return view('tag/data_insert');
    }

    /**
     * 修改
     * @param $id
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function data_update($id)
    {
        if (!$id) {
            $this->error('缺失参数');
        }
        $data = $this->tag_model->getOne(['id' => $id], '*', 'id desc');
        if (!$data) {
            $this->error('数据获取失败');
        }
        return view('tag/data_update', ['data' => $data]);
    }

    /**
     * 添加/修改 操作
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function data_save()
    {
        $data['title'] = input('post.title');
        $id            = input('post.id');

        $validate = new \app\common\validate\Tag();
        if (!$validate->check($data)) {
            return json(['code' => 100, 'message' => $validate->getError()]);
        }

        if ($id) {
            $res = $this->tag_model->editData(['id' => $id], $data);
        } else {
            $res = $this->tag_model->addData($data);
        }
        return operationResult($res, url("Tag/index"));
    }


    /**
     * 删除
     * @return \think\response\Json
     */
    public function data_delete()
    {
        $id = input('post.id');
        if (!$id) {
            return json(['code' => 100, 'msg' => '缺少参数ID']);
        }
        $res = $this->tag_model->dataDelete($id);
        return operationResult($res);
    }
}
