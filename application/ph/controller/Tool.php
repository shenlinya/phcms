<?php

/**
 * 插件中心
 */

namespace app\ph\controller;

use think\facade\Request;

class Tool extends Base
{
    // sitemap 存储路径
    protected $savePath;

    public function __construct()
    {
        parent::__construct();
        $this->savePath = './sitemap/';
    }

    /**
     * 插件中心
     * @return \think\response\View
     */
    public function index()
    {
        return view('tool/index');
    }

    /**
     * sitemap文件地图功能
     * @param string $path
     * @return \think\response\View
     */
    public function map($path = '')
    {
        if ($path) {
            //删除文件
            if (!file_exists($this->savePath . $path)) {
                $this->error('文件不存在');
            } else {
                unlink($this->savePath . $path);
                $this->success('删除成功');
            }
        }
        if (!file_exists($this->savePath)) {
            mkdir($path, 0755);
        }
        // 获取目录下文件
        $file = scandir($this->savePath);
        unset($file[0], $file[1]);
        $data  = [];
        $start = 1;
        foreach ($file as $k => $v) {
            $data[$k]['num']  = $start;
            $data[$k]['name'] = $v;
            $filePath         = './sitemap/' . $v;
            if (file_exists($filePath)) {
                $data[$k]['size'] = $this->getSize(filesize($filePath));
            } else {
                $data[$k]['size'] = '未找到文件';
            }
            $start += 1;
        }
        return view('tool/map',
            [
                'data' => $data
            ]
        );
    }

    /**
     * 文件大小格式
     * @param $fileSize
     * @return string
     */
    private function getSize($fileSize)
    {
        if ($fileSize >= 1073741824) {
            $fileSize = round($fileSize / 1073741824 * 100) / 100 . ' GB';
        } elseif ($fileSize >= 1048576) {
            $fileSize = round($fileSize / 1048576 * 100) / 100 . ' MB';
        } elseif ($fileSize >= 1024) {
            $fileSize = round($fileSize / 1024 * 100) / 100 . ' KB';
        } else {
            $fileSize = $fileSize . ' 字节';
        }
        return $fileSize;
    }

    /**
     * 百度站长普通收录配置
     * @return \think\response\Json|\think\response\View
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function push()
    {
        if (Request::isPost()) {
            $data['site']  = input('post.site');
            $data['token'] = input('post.token');
            $res           = \app\common\model\System::where(['id' => 1])->update($data);
            return operationResult($res);
        } else {
            $data = \app\common\model\System::where(['id' => 1])->field('site,token')->find();
            return view('tool/push',
                [
                    'data' => $data
                ]
            );
        }
    }

    /**
     * robots 设置
     * @return \think\response\View
     */
    public function robots()
    {
        $data = file_get_contents('./robots.txt');
        return view('tool/robots',
            [
                'data' => $data
            ]
        );
    }

    /**
     * robots保存
     * @return \think\response\Json
     */
    public function robotsSave()
    {
        $data = input('post.data');
        $res  = file_put_contents('./robots.txt', $data);
        return operationResult($res);
    }

    /**
     * 日志分析
     * @param $type
     * @return \think\response\View
     * @author shenlin
     * @time 2021/10/14 0014 16:54
     * @phone 13614048679
     */
    public function log($type)
    {
        $name = [
            'sql'   => ['name' => '数据库日志分析', 'tag' => '_sql.log'],
            'error' => ['name' => '错误日志分析', 'tag' => '_error.log'],
            'visit' => ['name' => '访问日志分析', 'tag' => ''],
        ];
        if (!array_key_exists($type, $name)) {
            abort(404);
        }

        $data = [];
        // 使用
        $glob = $this->glob2foreach('./runtime/log');
        while ($glob->valid()) {
            // 当前文件
            $filename = $glob->current();
            $tag      = $name[$type]['tag'];
            if ($tag) {
                strpos($filename, $name[$type]['tag']) && array_push($data, $filename);
            } else {
                if (!strpos($filename, '_error') && !strpos($filename, '_sql')) {
                    array_push($data, $filename);
                }
            }
            // 指向下一个，不能少
            $glob->next();
        }
        $content = [];
        $path    = input('get.path');
        if ($path) {
            // 使用
            $glob = $this->read_file($path);
            while ($glob->valid()) {
                // 当前行文本
                $line = $glob->current();
                // 逐行处理数据
                array_push($content, $line);
                // 指向下一个，不能少
                $glob->next();
            }
        }
        return view('tool/log',
            [
                'data'    => $data,
                'content' => implode("\n", $content),
                'path'    => $path,
                'catName' => $name[$type]['name']
            ]
        );
    }

    /**
     * 读取文件
     * @param $path
     * @return \Generator
     * @author shenlin
     * @time 2021/10/14 0014 16:20
     * @phone 13614048679
     */
    private function read_file($path)
    {
        if ($handle = fopen($path, 'r')) {
            while (!feof($handle)) {
                yield trim(fgets($handle));
            }
            fclose($handle);
        }
    }


    /**
     * 遍历目录
     * @param $path
     * @param bool $include_dirs 是否包含目录
     * @return \Generator
     * @author shenlin
     * @time 2021/10/14 0014 16:00
     * @phone 13614048679
     */
    private function glob2foreach($path, $include_dirs = false)
    {
        $path = rtrim($path, '/*');
        if (is_readable($path)) {
            $dh = opendir($path);
            while (($file = readdir($dh)) !== false) {
                if (substr($file, 0, 1) == '.')
                    continue;
                $rFile = "{$path}/{$file}";
                if (is_dir($rFile)) {
                    $sub = $this->glob2foreach($rFile, $include_dirs);
                    while ($sub->valid()) {
                        yield $sub->current();
                        $sub->next();
                    }
                    if ($include_dirs)
                        yield $rFile;
                } else {
                    yield $rFile;
                }
            }
            closedir($dh);
        }
    }

    /**
     * 绑定站点
     * @return \think\response\Json|\think\response\View
     * @author shenlin
     * @time 2021/10/28 0028 11:19
     * @phone 13614048679
     */
    public function binding()
    {
        if (Request::isAjax()) {
            $bindingType    = input('post.type');
            $bindingContent = input('post.content');
            cache('bindingType', $bindingType, 3600);
            cache('bindingContent', $bindingContent, 3600);
            return json(['code' => 200, 'message' => '保存成功']);
        } else {
            $bindingType    = cache('bindingType');
            $bindingContent = cache('bindingContent');
            return view('tool/binding',
                [
                    'type'    => $bindingType,
                    'content' => $bindingContent,
                ]
            );
        }
    }
}
