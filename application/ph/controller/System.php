<?php

/**
 * 系统设置
 */

namespace app\ph\controller;

class System extends Base
{
    protected $system_model;

    public function __construct()
    {
        parent::__construct();
        $this->system_model = new \app\common\model\System();
    }

    /**
     * 设置首页
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $data = $this->system_model->getOne(['id' => 1], '*');
        if (!$data) {
            $this->system_model->addData(['create_time' => time()]);
            $data = $this->system_model->getOne(['id' => 1], '*');
        }
        return view('system/index',
            [
                'data' => $data,
                'path' => config('path.randPath')
            ]
        );
    }

    /**
     * 数据操作
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function data_save()
    {
        $data['title']            = input('post.title');
        $data['brand']            = input('post.brand');
        $data['host']             = input('post.host');
        $data['keywords']         = input('post.keywords');
        $data['represent']        = input('post.represent');
        $data['logo']             = input('post.logo');
        $data['copyright']        = input('post.copyright');
        $data['icp']              = input('post.icp');
        $data['qq']               = input('post.qq');
        $data['we_chat']          = input('post.we_chat');
        $data['we_chat_img']      = input('post.we_chat_img');
        $data['address']          = input('post.address');
        $data['email']            = input('post.email');
        $data['template']         = input('post.template');
        $data['baidu_ai_key']     = input('post.baidu_ai_key');
        $data['baidu_ai_secret']  = input('post.baidu_ai_secret');
        $data['zip_code']         = input('post.zip_code');
        $data['statistical_code'] = htmlspecialchars_decode(input('post.statistical_code'));
        $data['tel']              = input('post.tel');
        $path                     = input('post.path');
        $id                       = input('post.id');
        // 替换主题路径
        $this->updateViewPath($data['template']);
        $validate = new \app\common\validate\System();
        if (!$validate->check($data)) {
            return json(['code' => 100, 'message' => $validate->getError()]);
        }

        if ($id) {
            $data['update_time'] = time();
            $res                 = $this->system_model->editData(['id' => $id], $data);
        } else {
            $res = $this->system_model->addData($data);
        }
        cache("system", $data);
        // 后台地址
        if ($path !== config('path.randPath')) {
            $this->updateRandPath($path);
            session(null, 'phCms');
        }
        return operationResult($res, url('System/index'));
    }

    /**
     * 替换后台入口
     * @param $content
     * @return void
     * @author shenlin
     * @time 2021/10/27 0027 12:03
     * @phone 13614048679
     */
    public function updateRandPath($content)
    {
        $path    = config('path.randPath');
        $appPath = \think\facade\Env::get('config_path') . 'path.php';
        $data    = file_get_contents($appPath);
        $data    = str_replace($path, $content, $data);
        file_put_contents($appPath, $data);
    }

    /**
     * 替换主题路径
     * @param $status
     * @return void
     * @author shenlin
     * @time 2022/3/15 0015 13:43
     * @phone 13614048679
     */
    public function updateViewPath($status)
    {
        $oldPath = env('TP_PATH');
        if ($oldPath !== $status) {
            $appPath = '../.env';
            $data    = file_get_contents($appPath);
            $data    = str_replace($oldPath, $status, $data);
            file_put_contents($appPath, $data);
        }
    }
}
