<?php

/**
 * 运营管理
 */
namespace app\ph\controller;

class Admin extends Base
{
    protected $admin_model;

    public function __construct()
    {
        parent::__construct();
        $this->admin_model = new \app\common\model\Admin();
    }

    /**
     * 列表功能
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $data = $this->admin_model->getList([]);
        $page = $data->render();
        $data = $data->items();
        return view('admin/index',
            [
                'data' => $data,
                'page' => $page
            ]
        );
    }

    /**
     * 添加
     * @return \think\response\View
     */
    public function data_insert()
    {
        return view('admin/data_insert');
    }

    /**
     * 修改
     * @param $id
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function data_update($id)
    {
        if (!$id) {
            $this->error('缺失参数ID');
        }
        $data = $this->admin_model->getOne(['id' => $id], '*', 'id desc');
        if (!$data) {
            $this->error('获取信息失败');
        }
        return view('admin/data_update',
            [
                'data' => $data
            ]
        );
    }

    /**
     * 数据操作
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function data_save()
    {
        $data['name'] = input('post.name');
        input('post.pass') && $data['pass'] = getPassRule(input('post.pass'));
        $data['status'] = input('post.status');
        $data['type']   = input('post.type');
        $id             = input('post.id');

        $validate = new \app\common\validate\Admin();
        if (!$validate->check($data)) {
            return json(['code' => 100, 'message' => $validate->getError()]);
        }

        if ($id) {
            $res = $this->admin_model->editData(['id' => $id], $data);
        } else {
            $res = $this->admin_model->addData($data);
        }
        return operationResult($res, url('Admin/index'));
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function data_delete()
    {
        $id = input('post.id');
        if (!$id) {
            return json(['code' => 100, 'message' => '缺失ID参数']);
        }
        $res = $this->admin_model->dataDelete($id);
        return operationResult($res);
    }
}
