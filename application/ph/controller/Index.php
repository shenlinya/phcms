<?php

/**
 * 首页管理
 */

namespace app\ph\controller;

use app\common\model\Record;
use think\Env;

class Index extends Base
{
    protected $record_model;

    public function __construct()
    {
        parent::__construct();
        $this->record_model = new Record();
    }

    /**
     * 系统主页
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        // 登录记录
        $record = $this->record_model->getAll([], 'create_time,ip');
        // 服务器域名
        $serverDomainName = $_SERVER['SERVER_NAME'];
        // IP地址
        $ip = $_SERVER['SERVER_NAME'];
        // 操作系统
        $os = explode(" ", php_uname());
        if ('/' == DIRECTORY_SEPARATOR) {
            $edition = $os[2];
        } else {
            $edition = $os[1];
        }
        $operatingSystem = $os[0] . ' - 内核版本：' . $edition;
        // 系统语言
        $systemLanguage = getenv("HTTP_ACCEPT_LANGUAGE");
        // 服务器路径
        $serverPath = $_SERVER['DOCUMENT_ROOT'] ? str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) : str_replace('\\', '/', dirname(__FILE__));
        // 服务器时间
        $serverTime = date("Y-m-d H:i:s", time());
        // PHP 版本
        $edition = PHP_VERSION;
        // CURL支持
        $curlSupport = $this->isFun("curl_init");
        // SMTP支持
        $smtpSupport = get_cfg_var("SMTP") ? '<font color="green">√</font>' : '<font color="red">×</font>';
        // 脚本占用最大内存
        $maximumMemoryUsedByScript = $this->function_show("memory_limit");
        // 上传文件限制
        $uploadFileLimit = $this->function_show("upload_max_filesize");
        // GD 库支持
        if (function_exists('gd_info')) {
            $gd_info = @gd_info();
            $gd      = $gd_info["GD Version"];
        } else {
            $gd = '×';
        };
        return view('index/index',
            [
                'record'                    => $record,
                'serverDomainName'          => $serverDomainName,
                'ip'                        => $ip,
                'operatingSystem'           => $operatingSystem,
                'systemLanguage'            => $systemLanguage,
                'serverPath'                => $serverPath,
                'serverTime'                => $serverTime,
                'edition'                   => $edition,
                'curlSupport'               => $curlSupport,
                'smtpSupport'               => $smtpSupport,
                'maximumMemoryUsedByScript' => $maximumMemoryUsedByScript,
                'uploadFileLimit'           => $uploadFileLimit,
                'gd'                        => $gd,
            ]
        );
    }

    /**
     * CURL支持
     * @param string $funName
     * @return string
     */
    public function isFun($funName = '')
    {
        if (!$funName || trim($funName) == '' || preg_match('~[^a-z0-9\_]+~i', $funName, $tmp)) return '错误';
        return (false !== function_exists($funName)) ? '<font color="green">√</font>' : '<font color="red">×</font>';
    }

    /**
     * 函数判断
     * @param $varName
     * @return string
     */
    public function function_show($varName)
    {
        switch ($result = get_cfg_var($varName)) {
            case 0:
                return '<font color="red">×</font>';
                break;
            case 1:
                return '<font color="green">√</font>';
                break;
            default:
                return $result;
                break;
        }
    }

    /**
     * debug调试
     * @return \think\response\Json
     */
    public function debug()
    {
        $status = input('post.status');
        if ($status === 'false') {
            // 关闭
            $content         = "TP_ENV = ONLINE";
            $templateContent = "TP_ENV = TEST";
        } else {
            // 开启
            $content         = "TP_ENV = TEST";
            $templateContent = "TP_ENV = ONLINE";
        }

        $appPath = '../.env';
        $data    = file_get_contents($appPath);
        $data    = str_replace($templateContent, $content, $data);
        file_put_contents($appPath, $data);
        return json(['code' => 200, 'message' => '操作成功']);
    }
}
