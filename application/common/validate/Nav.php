<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class Nav extends Validate
{
    protected $rule = [
        'cat_title' => 'require|unique:nav|max:32',
    ];

    protected $message = [
        'cat_title.require' => '栏目名称必须填写',
        'cat_title.max'     => '栏目名称最多不能超过32个字符',
        'cat_title.unique'  => '栏目名称已存在',
    ];
}
