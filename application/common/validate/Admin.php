<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class Admin extends Validate
{
    protected $rule = [
        'name'   => 'require|max:32',
        'pass'   => 'max:32',
        'status' => 'require|number|between:0,1',
        'type'   => 'require|number|between:0,1',
    ];

    protected $message = [
        'name.require'   => '姓名必须填写',
        'name.max'       => '姓名最多不能超过32个字符',
        'pass.max'       => '密码最多不能超过32个字符',
        'status.require' => '账户状态必须填写',
        'status.number'  => '账户状态必须是数字',
        'status.between' => '账户状态只能在0-1之间',
        'type.require'   => '账户类型必须填写',
        'type.number'    => '账户类型必须是数字',
        'type.between'   => '账户类型只能在0-1之间',
    ];
}
