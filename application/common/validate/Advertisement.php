<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class Advertisement extends Validate
{
    protected $rule = [
        'link'    => 'require|max:255',
        'content' => 'max:255',
        'num'     => 'require|number|between:0,100',
        'path'    => 'require|max:255',
    ];

    protected $message = [
        'link.require' => '链接必须填写',
        'link.max'     => '链接最多不能超过255个字符',
        'content.max'  => '备注最多不能超过255个字符',
        'num.require'  => '排序必须填写',
        'num.number'   => '排序必须是数字',
        'num.between'  => '排序只能在0-100之间',
        'path.require' => '路径必须填写',
        'path.max'     => '路径最多不能超过255个字符',
    ];
}
