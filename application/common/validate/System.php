<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class System extends Validate
{
    protected $rule = [
        'title'     => 'max:255',
        'keywords'  => 'max:255',
        'represent' => 'max:255',
        'logo'      => 'max:255',
        'copyright' => 'max:255',
        'icp'       => 'max:32',
        'tel'       => 'max:32',
        'we_chat'   => 'max:32',
        'qq'        => 'max:32',
        'host'      => 'max:255',
        'brand'     => 'max:32',
        'token'     => 'max:32',
        'site'      => 'max:255',
    ];

    protected $message = [
        'title.max'     => '网站标题最多可输入255个字符',
        'keywords.max'  => '关键字最多可输入32个字符',
        'represent.max' => '描述最多可输入255个字符',
        'logo.max'      => 'LOGO最多可输入255个字符',
        'copyright.max' => '版权链接最多可输入255个字符',
        'icp.max'       => '备案号最多可输入32个字符',
        'tel.max'       => '电话最多可输入32个字符',
        'we_chat.max'   => '微信最多可输入32个字符',
        'qq.max'        => 'QQ最多可输入32个字符',
        'host.max'      => '主页地址最多可输入255个字符',
        'brand.max'     => '品牌名最多可输入32个字符',
        'token.max'     => 'token最多可输入32个字符',
        'site.max'      => '域名最多可输入255个字符',
    ];
}
