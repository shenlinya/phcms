<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class Link extends Validate
{
    protected $rule = [
        'title'  => 'require|max:32',
        'link'   => 'require|max:255',
        'num'    => 'require|number|between:1,100',
        'status' => 'require|number|between:0,1',
    ];

    protected $message = [
        'title.require'  => '名称必须填写',
        'title.max'      => '名称最多不能超过32个字符',
        'link.require'   => '链接必须填写',
        'link.max'       => '链接最多不能超过255个字符',
        'num.require'    => '排序必须填写',
        'num.number'     => '排序必须是数字',
        'num.between'    => '排序只能在1-100之间',
        'status.require' => '显示状态必须填写',
        'status.number'  => '显示状态必须是数字',
        'status.between' => '显示状态只能在0-1之间',
    ];
}
