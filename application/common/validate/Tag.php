<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class Tag extends Validate
{
    protected $rule = [
        'title' => 'require|unique:tag|max:32',
    ];

    protected $message = [
        'title.require' => '名称必须填写',
        'title.max'     => '名称最多不能超过32个字符',
        'title.unique'  => '名称已存在',
    ];
}
