<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class Slide extends Validate
{
    protected $rule = [
        'num'     => 'require|number|between:1,100',
        'link'    => 'require|max:255',
        'path'    => 'require|max:255',
        'content' => 'max:255',
    ];

    protected $message = [
        'num.require'  => '排序必须填写',
        'num.number'   => '排序类型必须为数字',
        'num.between'  => '排序至在1-100之间',
        'link.require' => '链接必须填写',
        'link.max'     => '链接最多可输入255个字符',
        'path.require' => '路径必须填写',
        'path.max'     => '路径最多可输入255个字符',
        'content.max'  => '备注最多可输入255个字符',
    ];
}
