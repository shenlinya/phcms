<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/10/31 0031
 * Time: 10:35
 */

namespace app\common\validate;

use think\Validate;

class Message extends Validate
{
    protected $rule = [
        'qq'      => 'number|max:11',
        'nick'    => 'require|max:32',
        'content' => 'require|max:255',
    ];

    protected $message = [
        'qq.number'       => 'QQ格式输入错误',
        'qq.max'          => 'QQ格式最多可输入11个字符',
        'nick.require'    => '姓名必须填写',
        'nick.max'        => '姓名最多不能超过32个字符',
        'content.require' => '留言内容最多可输入200个字符',
        'content.max'     => '留言内容最多可输入200个字符',
    ];
}
