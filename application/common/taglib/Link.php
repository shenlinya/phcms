<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/22 0022 13:42
 */

namespace app\common\taglib;

use think\template\TagLib;

class Link extends TagLib
{
    // 定义标签列表
    protected $tags = [
        'open' => ['attr' => 'where,field,order,limit', 'close' => 1],
    ];

    /**
     * 友情链接
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2021/10/22 0022 15:54
     * @phone 13614048679
     */
    public function tagOpen($tag, $content)
    {
        $parse = '<?php $map=[];';
        $parse .= '$__LIST__ = Db::name("link")';
        $parse .= '->where("delete_time IS NULL")';
        if (isset($tag['where']) && $tag['where']) {
            $parse .= '->where(' . $tag['where'] . ')';
        }
        if (isset($tag['field']) && $tag['field']) {
            $parse .= '->field("' . $tag['field'] . '")';
        } else {
            $parse .= '->field("id,title,link")';
        }
        if (isset($tag['order']) && $tag['order']) {
            $parse .= '->order(' . $tag['order'] . ')';
        } else {
            $parse .= '->order("num ASC")';
        }
        if (isset($tag['limit']) && $tag['limit']) {
            $parse .= '->limit(' . $tag['limit'] . ')';
        } else {
            $parse .= '->limit(10)';
        }
        $parse .= '->select(); ?>';
        $parse .= '{volist name="__LIST__" id="v"}';
        $parse .= $content;
        $parse .= "{/volist}";
        return $parse;
    }
}
