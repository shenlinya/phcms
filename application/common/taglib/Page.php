<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/22 0022 13:42
 */

namespace app\common\taglib;

use think\template\TagLib;

class Page extends TagLib
{
    // 定义标签列表
    protected $tags = [
        'open' => ['attr' => 'where,field,start,length', 'close' => 0],
    ];

    /**
     * 标签
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2021/10/22 0022 15:54
     * @phone 13614048679
     */
    public function tagOpen($tag, $content)
    {
        $parse = '<?php $info = Db::name("page")';
        $parse .= '->where("delete_time IS NULL")';
        $parse .= '->where("' . $tag['where'] . '")';
        $parse .= '->order("id DESC")';
        $parse .= '->value("' . $tag['field'] . '");';
        if (isset($tag['start'])) {
            if (!isset($tag['length'])) $tag['length'] = 1;
            $parse .= 'echo strip_tags(mb_substr($info,' . $tag['start'] . ',' . $tag['length'] . ')); ?>';
        } else {
            $parse .= 'echo $info; ?>';
        }
        return $parse;
    }
}
