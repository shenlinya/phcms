<?php

/**
 *
 * @author     shenlin
 * @ctime:     2021/10/22 0022 13:42
 */

namespace app\common\taglib;


use think\Db;
use think\template\TagLib;

class Content extends TagLib
{
    // 定义标签列表
    protected $tags = [
        'top'       => ['attr' => 'where,filed,limit,offset,length', 'close' => 1],
        'hot'       => ['attr' => 'limit', 'close' => 1],
        'recommend' => ['attr' => 'where,filed,limit', 'close' => 1],
        'new'       => ['attr' => 'where,filed,limit', 'close' => 1],
        'rand'      => ['attr' => 'where,filed,limit', 'close' => 1],
        'count'     => ['attr' => 'where', 'close' => 0],
    ];


    /**
     * 置顶内容
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2022/2/16 0016 15:50
     * @phone 13614048679
     */
    public function tagTop($tag, $content)
    {
        $parse = '<?php $map=[];';
        $parse .= '$__LIST__ = Db::name("content")->where("top=1 AND status=1 AND delete_time IS NULL")';
        if (isset($tag['where']) && $tag['where']) {
            $parse .= '->where("' . $tag['where'] . '")';
        }
        if (isset($tag['field']) && $tag['field']) {
            $parse .= '->field("' . $tag['field'] . '")';
        }
        if (isset($tag['limit']) && $tag['limit']) {
            $parse .= '->limit(' . $tag['limit'] . ')';
        }
        $parse .= '->order("id DESC")';
        $parse .= '->select(); ?>';
        if (isset($tag['offset']) && $tag['offset']) {
            if (!isset($tag['length']) || !$tag['length']) {
                $tag['length'] = 1;
            }
            $parse .= '{volist name="__LIST__" id="v" offset="' . $tag['offset'] . '" length="' . $tag['length'] . '"}';
        } else {
            $parse .= '{volist name="__LIST__" id="v"}';
        }
        $parse .= $content;
        $parse .= "{/volist}";
        return $parse;
    }

    /**
     * 热门内容
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2022/2/16 0016 15:50
     * @phone 13614048679
     */
    public function tagHot($tag, $content)
    {
        $parse = '<?php $map=[];';
        $parse .= '$__LIST__ = Db::name("content")->where("top=1 AND status=1 AND delete_time IS NULL")';
        if (isset($tag['where']) && $tag['where']) {
            $parse .= '->where("' . $tag['where'] . '")';
        }
        if (isset($tag['field']) && $tag['field']) {
            $parse .= '->field("' . $tag['field'] . '")';
        }
        if (isset($tag['limit']) && $tag['limit']) {
            $parse .= '->limit(' . $tag['limit'] . ')';
        }
        $parse .= '->order("views DESC")';
        $parse .= '->select(); ?>';
        $parse .= '{volist name="__LIST__" id="v"}';
        $parse .= $content;
        $parse .= "{/volist}";
        return $parse;
    }

    /**
     * 推荐内容
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2022/2/16 0016 15:50
     * @phone 13614048679
     */
    public function tagRecommend($tag, $content)
    {
        $parse = '<?php $map=[];';
        $parse .= '$__LIST__ = Db::name("content")->where("recommend=1 AND status=1 AND delete_time IS NULL")';
        if (isset($tag['where']) && $tag['where']) {
            $parse .= '->where("' . $tag['where'] . '")';
        }
        if (isset($tag['field']) && $tag['field']) {
            $parse .= '->field("' . $tag['field'] . '")';
        }
        if (isset($tag['limit']) && $tag['limit']) {
            $parse .= '->limit(' . $tag['limit'] . ')';
        }
        $parse .= '->order("id DESC")';
        $parse .= '->select(); ?>';
        $parse .= '{volist name="__LIST__" id="v"}';
        $parse .= $content;
        $parse .= "{/volist}";
        return $parse;
    }

    /**
     * 最新内容
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2022/2/16 0016 15:50
     * @phone 13614048679
     */
    public function tagNew($tag, $content)
    {
        $parse = '<?php $map=[];';
        $parse .= '$__LIST__ = Db::name("content")->where("status=1 AND delete_time IS NULL")';
        if (isset($tag['where']) && $tag['where']) {
            $parse .= '->where("' . $tag['where'] . '")';
        }
        if (isset($tag['field']) && $tag['field']) {
            $parse .= '->field("' . $tag['field'] . '")';
        }
        if (isset($tag['limit']) && $tag['limit']) {
            $parse .= '->limit(' . $tag['limit'] . ')';
        }
        $parse .= '->order("id DESC")';
        $parse .= '->select(); ?>';
        $parse .= '{volist name="__LIST__" id="v"}';
        $parse .= $content;
        $parse .= "{/volist}";
        return $parse;
    }

    /**
     * 随机内容
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2022/2/16 0016 15:50
     * @phone 13614048679
     */
    public function tagRand($tag, $content)
    {
        $parse = '<?php $map=[];';
        $parse .= '$__LIST__ = Db::name("content")->where("rand=1 AND status=1 AND delete_time IS NULL")';
        if (isset($tag['where']) && $tag['where']) {
            $parse .= '->where("' . $tag['where'] . '")';
        }
        if (isset($tag['field']) && $tag['field']) {
            $parse .= '->field("' . $tag['field'] . '")';
        }
        if (isset($tag['limit']) && $tag['limit']) {
            $parse .= '->limit(' . $tag['limit'] . ')';
        }
        $parse .= '->order("id DESC")';
        $parse .= '->select(); ?>';
        $parse .= '{volist name="__LIST__" id="v"}';
        $parse .= $content;
        $parse .= "{/volist}";
        return $parse;
    }


    /**
     * 总数
     * @param $tag
     * @param $content
     * @return string
     * @author shenlin
     * @time 2022/2/16 0016 15:50
     * @phone 13614048679
     */
    public function tagCount($tag, $content)
    {
        $content = '<?php ';
        $content .= '$count = Db::name("content")->where("status=1 AND delete_time IS NULL")';
        if (isset($tag['where']) && $tag['where'] === 'month') {$content .= "->whereTime('create_time', 'month')";}
        if (isset($tag['where']) && $tag['where'] === 'today') {$content .= "->whereTime('create_time', 'today')";}
        $content .= '->count();';
        $content .= 'echo $count; ?>';
        return $content;
    }
}
