<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/9 0009
 * Time: 9:34
 */

namespace app\common\model;

use think\Model;
use think\model\concern\SoftDelete;

class Record extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';

    /**
     * 查询
     * @param $where
     * @param $field
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getAll($where, $field)
    {
        return $this->where($where)->field($field)->order('id desc')->limit(2)->select();
    }

    /**
     * 添加
     * @param $data
     * @return bool
     */
    public function addData($data)
    {
        return $this->save($data);
    }
}
