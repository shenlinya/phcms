<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/9 0009
 * Time: 9:34
 */

namespace app\common\model;

use think\Model;
use think\model\concern\SoftDelete;

class Link extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';

    /**
     * 分页列表
     * @param $where
     * @param string $field
     * @param string $order
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($where, $field = '*', $order = 'create_time desc')
    {
        return $this->where($where)->field($field)->order($order)->paginate();
    }

    /**
     * 查询一条
     * @param $where
     * @param $field
     * @param $order
     * @return array|null|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getOne($where, $field, $order)
    {
        return $this->where($where)->field($field)->order($order)->find();
    }

    /**
     * 查询
     * @param $where
     * @param $field
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getAll($where, $field)
    {
        return $this->where($where)->field($field)->order('num asc')->select();
    }

    /**
     * 添加
     * @param $data
     * @return bool
     */
    public function addData($data)
    {
        return $this->save($data);
    }

    /**
     * 修改
     * @param $where
     * @param $data
     * @return int|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function editData($where, $data)
    {
        return $this->where($where)->update($data);
    }

    /**
     * 删除
     * @param $id
     * @return bool
     */
    public function dataDelete($id)
    {
        if (!$id) {
            return false;
        }
        return $this->destroy($id);
    }
}
