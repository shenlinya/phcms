<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/9 0009
 * Time: 9:34
 */

namespace app\common\model;

use think\facade\Request;
use think\Model;
use think\model\concern\SoftDelete;

class Content extends Model
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';

    /**
     * 分页列表
     * @param        $where
     * @param string $field
     * @param string $order
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($where, $field = '*', $order = 'c.create_time desc')
    {
        return $this->alias(['content' => 'c'])->leftJoin(['nav' => 'n'], 'c.type = n.id')->where($where)->field($field)->order($order)->paginate(15, false, ['query' => request()->param()]);
    }

    /**
     * 分页列表
     * @param $path
     * @param $where
     * @param string $field
     * @param string $order
     * @return Content|\think\Paginator
     * @author shenlin
     * @time 2022/2/17 0017 13:06
     * @phone 13614048679
     */
    public function getIndexList($path, $where, $field = '*', $order = 'create_time desc')
    {
        return $this->where($where)->field($field)->order($order)->paginate(15, false, ['path' => Request::domain() . $path]);
    }


    /**
     * 分页列表
     * @param $where
     * @param string $field
     * @param string $order
     * @return Content|\think\Paginator
     * @author shenlin
     * @time 2022/2/17 0017 13:06
     * @phone 13614048679
     */
    public function getSearchList($where, $field = '*', $order = 'create_time desc')
    {
        return $this->where($where)->field($field)->order($order)->paginate(15, false, ['query' => Request::param()]);
    }

    /**
     * 查询一条
     * @param $where
     * @param $field
     * @param $order
     * @return array|null|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getOne($where, $field, $order)
    {
        return $this->where($where)->field($field)->order($order)->find();
    }

    /**
     * 查询
     * @param $where
     * @param $field
     * @param int $limit
     * @return Content[]|\think\Collection
     * @author shenlin
     * @time 2022/2/17 0017 9:49
     * @phone 13614048679
     */
    public function getAll($where, $field, $limit = 0)
    {
        if ($limit) {
            return $this->where($where)->field($field)->order('id desc')->limit($limit)->select();
        } else {
            return $this->where($where)->field($field)->order('id desc')->select();
        }
    }

    /**
     * 添加
     * @param $data
     * @return bool
     */
    public function addData($data)
    {
        return $this->save($data);
    }

    /**
     * 修改
     * @param $where
     * @param $data
     * @return int|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function editData($where, $data)
    {
        $data['update_time'] = time();
        return $this->where($where)->update($data);
    }

    /**
     * 删除
     * @param $id
     * @return bool
     */
    public function dataDelete($id)
    {
        if (!$id) {
            return false;
        }
        return $this->destroy($id);
    }

    /**
     * 数据自增
     * @param $id
     * @param $field
     * @return int|true
     * @throws \think\Exception
     */
    public function dataIns($id, $field)
    {
        return $this->where(['id' => $id])->setInc($field);
    }
}
