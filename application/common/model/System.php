<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/9 0009
 * Time: 9:34
 */

namespace app\common\model;

use think\Model;
use think\model\concern\SoftDelete;

class System extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';


    /**
     * 查询一条
     * @param $where
     * @param $field
     * @return array|null|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getOne($where, $field)
    {
        return $this->where($where)->field($field)->find();
    }

    /**
     * 添加
     * @param $data
     * @return bool
     */
    public function addData($data)
    {
        return $this->save($data);
    }

    /**
     * 修改
     * @param $where
     * @param $data
     * @return int|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function editData($where, $data)
    {
        return $this->where($where)->update($data);
    }
}
