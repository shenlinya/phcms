<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/9 0009
 * Time: 9:34
 */

namespace app\common\model;

use think\facade\Request;
use think\Model;
use think\model\concern\SoftDelete;

class Page extends Model
{
    use SoftDelete;

    protected $deleteTime = 'delete_time';

    /**
     * 分页列表
     * @param        $where
     * @param string $field
     * @param string $order
     * @return \think\Paginator
     * @throws \think\exception\DbException
     */
    public function getList($where, $field = '*', $order = 'p.create_time desc')
    {
        return $this->alias(['page' => 'p'])
            ->leftJoin(['nav' => 'n'], 'p.nav_id = n.id')
            ->where($where)
            ->field($field)
            ->order($order)
            ->paginate(15, false, ['query' => request()->param()]);
    }

    /**
     * 分页列表
     * @param $path
     * @param $where
     * @param string $field
     * @param string $order
     * @return Page|\think\Paginator
     * @author shenlin
     * @time 2021/8/27 0027 8:39
     * @phone 13614048679
     */
    public function getIndexList($path, $where, $field = '*', $order = 'create_time desc')
    {
        return $this->where($where)->field($field)->order($order)->paginate(15, false,
            [
                'path' => Request::domain() . '/' . $path . '/p[PAGE].html'
            ]
        );
    }

    /**
     * 查询一条
     * @param $where
     * @param $field
     * @param $order
     * @return Page
     * @author shenlin
     * @time 2021/8/27 0027 8:39
     * @phone 13614048679
     */
    public function getOne($where, $field, $order)
    {
        return $this->where($where)->field($field)->order($order)->find();
    }

    /**
     * 获取单值
     * @param $where
     * @param $field
     * @return mixed
     */
    public function getValue($where, $field)
    {
        return $this->where($where)->value($field);
    }

    /**
     * 查询
     * @param $where
     * @param $field
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getAll($where, $field)
    {
        return $this->where($where)->field($field)->order('id desc')->select();
    }

    /**
     * 文章详情页 相关推荐
     * @param $where
     * @param $field
     * @param $limit
     * @param string $order
     * @return Content[]|\think\Collection
     * @author shenlin
     * @time 2021/8/26 0026 17:08
     * @phone 13614048679
     */
    public function getIndexData($where, $field, $limit, $order = 'id desc')
    {
        return $this->where($where)->field($field)->order($order)->limit($limit)->select();
    }

    /**
     * 添加
     * @param $data
     * @return bool
     */
    public function addData($data)
    {
        return $this->save($data);
    }

    /**
     * 修改
     * @param $where
     * @param $data
     * @return int|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function editData($where, $data)
    {
        return $this->where($where)->update($data);
    }

    /**
     * 删除
     * @param $id
     * @return bool
     */
    public function dataDelete($id)
    {
        if (!$id) {
            return false;
        }
        return $this->destroy($id);
    }

    /**
     * 数据自增
     * @param $id
     * @param $field
     * @return int|true
     * @throws \think\Exception
     */
    public function dataIns($id, $field)
    {
        return $this->where(['id' => $id])->setInc($field);
    }
}
