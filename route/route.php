<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 新闻详情页
Route::get('/content/<id>', 'index/Content/index')->ext('html')->pattern(['id' => '\d+']);
// 单页面
Route::get('/page/<id>$', 'index/Page/index')->ext('html')->pattern(['id' => '\d+']);
// 栏目页
Route::get('/list/<id>-<page>$', 'index/Nav/index')->ext('html')->pattern(['id' => '\d+']);
// 标签列表页
Route::get('/tag/<id>-<page>$', 'index/Nav/tag')->ext('html')->pattern(['id' => '\d+']);
// 留言
Route::get('/message$', 'index/Message/index')->ext('html');
// 搜索
Route::get('/search$', 'index/Content/search')->ext('html');
